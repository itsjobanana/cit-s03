package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8405435963527493895L;


	public void init()throws ServletException {
		System.out.println("**********************************");
		System.out.println("Initialize connection to database.");
		System.out.println("**********************************");
		
	}
	
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		PrintWriter out = res.getWriter();
		
		int num1 = Integer.parseInt(req.getParameter("number1"));
		int num2 = Integer.parseInt(req.getParameter("number2"));
		String operation = req.getParameter("operation");
		
		int total = 0;
		
		
		if(operation.equals("add")) {
			
			total = num1 + num2;
				
		}
		else if(operation.equals("subtract")) {
			
			total = num1 - num2;
				
		}
		else if(operation.equals("multiply")) {
			
			total = num1 * num2;
			
		}
		
		else if(operation.equals("divide")) {
			
			total = num1 / num2;
			
		}
		
		out.printf("The two numbers you provided are: " +num1+ " , " +num2);
		out.printf("\n\nThe operation that you wanted is: " + operation);
		out.printf("\n\nThe result is: " + total);
	}
		
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		PrintWriter out = res.getWriter();

		out.println("<h1>You are now using the calculator app</h1>");
		out.println("To use the app, input 2 numbers and an operation.");
		out.println("<br><br>Hit the submission button after filling in the details.");
		out.println("<br><br>You will get the result shown in your browser!");
	}
	

	public void destroy() {
		
		System.out.println("***************************");
		System.out.println("Disconected from database.");
		System.out.println("***************************");
		
	}
	

}
